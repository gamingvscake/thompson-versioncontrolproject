/*
Player
 
 represents player (blue triangle) in game
 
 */

class Player {

  //three corner points for the triangle, starting from the top point going clockwise.
  PVector p1, p2, p3;

  //speed of the player in the x and y direction
  float speed = 4;
  //player will be rendered in teal
  color c = color(66, 245, 245);

  //there will be a cooldown for shooting, and number of bullets on the window will also be limited
  float bulletMax = 5, shootingCooldown, shootingCooldownMax = 0.1;

  //we will store all our bullets in this ArrayList
  ArrayList<Bullet> bullets;


  Player() {

    //initialize bullets ArrayList
    bullets = new ArrayList<Bullet>();

    //start the player at the bottom part of the screen, in the middle
    p1 = new PVector(250, 700);
    p2 = new PVector(230, 750);
    p3 = new PVector(270, 750);
    shootingCooldown = shootingCooldownMax;
  }



  //update function will take in how much on x and y axis the player will move
  void update(float x, float y) {

    //we multiply the arguments with player speed to find the offset by which we will move the player
    float newX = x * speed;
    float newY = y * speed;

    //if the new position after we move the player will be outside the window boundaries, we set the movement offset to 0 on X and Y axes respectively. 
    if (p2.x + newX <= 0 || p3.x + newX >= 500) {
      newX = 0;
    } 

    if (p1.y + newY <= 0 || p2.y + newY >= 800) {
      newY = 0;
    }

    //assemble a PVector from the calculated offsets for ease of use
    PVector pSpeed = new PVector(newX, newY);

    //add the speed PVector to all points to move them by the offset on both axes 
    p1 = PVector.add(p1, pSpeed);
    p2 = PVector.add(p2, pSpeed);
    p3 = PVector.add(p3, pSpeed);

    //reduce shootingCooldown. we are using 1/frameRate to use second as the unit for the cooldown values 
    shootingCooldown -= (1/ frameRate);
