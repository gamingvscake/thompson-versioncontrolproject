/*
 
 Most of the game logic is handled here. 
 
 //hello from Carlton
 
 //hi
 
 */



class GameManager {

  //Stores references to the player, enemies, background. Also contains a class which stores functions used for collision detection.
  Player p;
  ArrayList<Enemy> enemies;
  BackGround bg;
  CollisionFunctions cf = new CollisionFunctions();

  //if set to true, game will stop progressing
  boolean gameOver = false;


  //We will use a boolean to determine which keys are pressed down on each frame. We use this method for smooth, diagonal movement
  //SpaceBar/Enter, leftArrow/A, upArrow/W, rightArrow/D, downArrow/S
  boolean[] keys = new boolean[5];


  GameManager(int numEnemies) {

    //initialize player
    p = new Player();

    //initialize enemies and add them to the arraylist
    enemies = new ArrayList<Enemy>();
    for (int i = 0; i < numEnemies; i++) {
      enemies.add(new Enemy());
    }

    //initialize background with the number of stars desired
    bg = new BackGround(64);
  }

  //runs every frame and will update and process all objects in the game
  void update() {
    //update the background
    bg.update();

    //calculate and send the input values to the player
    handleInput();


    //process each enemy
    for (int i = 0; i < enemies.size(); i++) {
      //move the enemy
      enemies.get(i).move();

      //check collisions or whether enemy is off screen
      processEnemyPosition(enemies.get(i));
    }
  }
  void render() {
    //render player
    p.render();

    //render each enemy
    for (int i = 0; i < enemies.size(); i++) {    
      enemies.get(i).render();
    }
  }

  //calculates input values to be used by the player object. also handles shooting
  void handleInput() {
    int xDiff =0, yDiff = 0;

    if (keys[0])//spacebar/enter
    {
      p.shoot();
    }

    if (keys[2])//up/w
    {
      yDiff += -1;
    }

    if (keys[4])//down/s
    {
      yDiff += 1;
    }

    if (keys[1])//left/a
    {
      xDiff += -1;
    }

    if (keys[3])//right/d
    {
      xDiff += 1;
    }

    //send the final values to the player
    p.update(xDiff, yDiff);
  }


  void processEnemyPosition(Enemy e) {
    //Does it collide with the player?
    if (cf.polyPoly(e.getVertices(), p.getVertices())) {
      //if yes, stop the game
      gameOver = true;
      return; //stop processing the rest of the function
    }

    //Does it collide with a bullet?
    //check each bullet using a for look
    for (int i = 0; i < p.bullets.size(); i++) {
      if (checkEnemyBulletCollision(e, p.bullets.get(i))) {
        //if yes, respawn enemy
        e.initialize();
        return; //stop processing the rest of the function
      }
    }

    //Is it at the edge of the screen?
    if (e.p2.y >= height) {
      //if yes, respawn the enemy
      e.initialize();
    }
  }

  //checks enemy vs bullet collision
  boolean checkEnemyBulletCollision(Enemy e, Bullet b) {
    return cf.polyCircle(e.getVertices(), b.posn.x, b.posn.y, b.size.x);
  }






  //All functions below are taken from http://www.jeffreythompson.org and modified to fit our needs
  class CollisionFunctions
  {
    // POLYGON/POLYGON
    boolean polyPoly(PVector[] p1, PVector[] p2) {

      // go through each of the vertices, plus the next
      // vertex in the list
      int next = 0;
      for (int current=0; current<p1.length; current++) {

        // get next vertex in list
        // if we've hit the end, wrap around to 0
        next = current+1;
        if (next == p1.length) next = 0;

        // get the PVectors at our current position
        // this makes our if statement a little cleaner
        PVector vc = p1[current];    // c for "current"
        PVector vn = p1[next];       // n for "next"

        // now we can use these two points (a line) to compare
        // to the other polygon's vertices using polyLine()
        boolean collision = polyLine(p2, vc.x, vc.y, vn.x, vn.y);
        if (collision) return true;

        // optional: check if the 2nd polygon is INSIDE the first
        collision = polyPoint(p1, p2[0].x, p2[0].y);
        if (collision) return true;
      }

      return false;
    }
    // POLYGON/LINE
    boolean polyLine(PVector[] vertices, float x1, float y1, float x2, float y2) {

      // go through each of the vertices, plus the next
      // vertex in the list
      int next = 0;
      for (int current=0; current<vertices.length; current++) {

        // get next vertex in list
        // if we've hit the end, wrap around to 0
        next = current+1;
        if (next == vertices.length) next = 0;

        // get the PVectors at our current position
        // extract X/Y coordinates from each
        float x3 = vertices[current].x;
        float y3 = vertices[current].y;
        float x4 = vertices[next].x;
        float y4 = vertices[next].y;

        // do a Line/Line comparison
        // if true, return 'true' immediately and
        // stop testing (faster)
        boolean hit = lineLine(x1, y1, x2, y2, x3, y3, x4, y4);
        if (hit) {
          return true;
        }
      }

      // never got a hit
      return false;
    }
