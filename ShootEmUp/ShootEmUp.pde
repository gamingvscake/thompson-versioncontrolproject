/*
Shoot'em'up style interactive, object oriented toy
Main sketch will handle input and main frame
*/

//GameManager object will contain most of our game logic
GameManager gm;

void setup() {
  size(500, 800);
  //start the game with 7 enemies
  gm = new GameManager(7);
}

void draw() {
  //will not do anything if game over
  if (gm.gameOver)
    return;

  //clear the screen and call Game Manager's update and draw functions
  background(0);
  gm.update();
  gm.render();
}
